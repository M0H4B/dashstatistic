﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class UserControl2 : UserControl
    {
        DataClasses1DataContext db;
        public UserControl2()
        {
            InitializeComponent();
            db = new DataClasses1DataContext(Static.ConnectionString);
            ultraCalendarInfo1.Owners.UnassignedOwner.Visible = false;

            tASKBindingSource.DataSource = db.TASKs.AsQueryable();
            this.ultraCalendarInfo1.DataBindingsForOwners.BindingContextControl = this;

            this.ultraCalendarInfo1.DataBindingsForOwners.DataSource = tASKBindingSource.DataSource;

            //this.ultraCalendarInfo1.DataBindingsForOwners.EmailAddressMember = "ID";

            this.ultraCalendarInfo1.DataBindingsForOwners.KeyMember = "TASK_CODE";

            this.ultraCalendarInfo1.DataBindingsForOwners.NameMember = "TASK_NAME";
            this.ultraCalendarInfo1.DataBindingsForOwners.Tag = "ID";

            
            this.ultraCalendarInfo1.DataBindingsForAppointments.DataSource = aPPOINTMENTBindingSource;


            this.ultraCalendarInfo1.DataBindingsForAppointments.SubjectMember = "APPOINTMENT_NAME";

            this.ultraCalendarInfo1.DataBindingsForAppointments.DataKeyMember = "APPOINTMENT_CODE";

            this.ultraCalendarInfo1.DataBindingsForAppointments.EndDateTimeMember = "DUE_DATE";

            this.ultraCalendarInfo1.DataBindingsForAppointments.StartDateTimeMember = "INSERT_DATE";

            this.ultraCalendarInfo1.DataBindingsForAppointments.DescriptionMember = "APPOINTMENT_NOTE";

            this.ultraCalendarInfo1.DataBindingsForAppointments.OwnerKeyMember = "TASK_CODE";
            this.ultraCalendarInfo1.DataBindingsForAppointments.Tag = "ID";

            ultraDayView1.CalendarInfo = ultraCalendarInfo1;
            ultraMonthViewSingle1.CalendarInfo = ultraCalendarInfo1;
            ultraWeekView1.CalendarInfo = ultraCalendarInfo1;
            ultraMonthViewSingle1.ShowOwnerHeader = Infragistics.Win.DefaultableBoolean.True;
            ultraDayView1.ShowOwnerHeader = Infragistics.Win.DefaultableBoolean.True;
            ultraWeekView1.ShowOwnerHeader = Infragistics.Win.DefaultableBoolean.True;
            button2_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ultraDayView1.Visible = true;
            ultraMonthViewSingle1.Visible = false;
            ultraWeekView1.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ultraDayView1.Visible = false;
            ultraMonthViewSingle1.Visible = false;
            ultraWeekView1.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ultraDayView1.Visible = false;
            ultraMonthViewSingle1.Visible = true;
            ultraWeekView1.Visible = false;
        }


        APPOINTMENT last;
        private void ultraCalendarInfo1_AfterAppointmentAdded(object sender, Infragistics.Win.UltraWinSchedule.AppointmentEventArgs e)
        {
            Guid gg=Guid.NewGuid();
            try
            {
                last = new APPOINTMENT()
            {
                ID = gg,
                TASK_ID = Guid.Parse(e.Appointment.Owner.Key),
                TASK_CODE=e.Appointment.OwnerKey,
                APPOINTMENT_CODE=gg.ToString(),
                IS_DELETED = false,
                IS_DONE = false,
                INSERT_DATE = e.Appointment.Start,
                DUE_DATE = e.Appointment.End,
                APPOINTMENT_NAME = e.Appointment.Subject,
                APPOINTMENT_NOTE = e.Appointment.Description,
                NOTIFY = true

            };
                db.APPOINTMENTs.InsertOnSubmit(last);
                db.SubmitChanges();
            }
            catch { }
        }

        private void ultraCalendarInfo1_AfterAppointmentRemoved(object sender, EventArgs e)
        {
            try
            {
                db.SubmitChanges();
            }
            catch { }
        }

       

        private void aPPOINTMENTBindingSource_AddingNew(object sender, AddingNewEventArgs e)
        {
           e.NewObject=last;
           last = null;
        }

    }
}
