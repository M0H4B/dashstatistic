﻿namespace dashstatistic
{
    partial class noteControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnote = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblnote
            // 
            this.lblnote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblnote.Font = new System.Drawing.Font("Segoe UI Symbol", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnote.ForeColor = System.Drawing.Color.Red;
            this.lblnote.Location = new System.Drawing.Point(0, 0);
            this.lblnote.Margin = new System.Windows.Forms.Padding(0);
            this.lblnote.Name = "lblnote";
            this.lblnote.Size = new System.Drawing.Size(290, 26);
            this.lblnote.TabIndex = 1;
            this.lblnote.Text = "تفقد تاريخ صلاحيه المنتجات";
            this.lblnote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // noteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lblnote);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "noteControl";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(290, 26);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblnote;
    }
}
