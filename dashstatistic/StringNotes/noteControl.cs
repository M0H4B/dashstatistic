﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class noteControl : UserControl
    {
        public noteControl(DATA_KEYPAIR_NOTE note)
        {
            InitializeComponent();
            lblnote.Text = note.DATA_NOTE;
            lblnote.ForeColor = System.Drawing.ColorTranslator.FromHtml(note.STATISTIC_TYPE.BODY_COLOR);
             var cvt = new FontConverter();
            lblnote.Font = cvt.ConvertFromString(note.STATISTIC_TYPE.BODY_FONT) as Font;
        }
    }
}
