﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class stringnotes : UserControl
    {
        public stringnotes(List<DATA_KEYPAIR_NOTE> data)
        {
            InitializeComponent();


            // Make your controls movable by a mouseclick
           // ControlMover.Init(this.button1);
           //ControlMover.Init(this.checkBox1);
           // ControlMover.Init(this.groupBox1);
           // ControlMover.Init(this.textBox1);
           // ControlMover.Init(this.label1);

           // // Move a panel by its toolstrip
           // ControlMover.Init(this.toolStrip2, this.panel3,
           //                          Helper.ControlMover.Direction.Any);

           // // Make a splitter from toolstrip
           //ControlMover.Init(this.toolStrip1, Helper.ControlMover.Direction.Vertical);
           // this.toolStrip1.LocationChanged += delegate(object sender, EventArgs e)
           // {
           //     this.panel1.Height = this.toolStrip1.Top;
           // };

            foreach (DATA_KEYPAIR_NOTE item in data)
            {
                Control ctrl = new noteControl(item);
                ctrl.Dock = DockStyle.Top;
                this.panel1.Controls.Add(ctrl);
               
            }

        }
    }
}
