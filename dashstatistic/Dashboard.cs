﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Infragistics.Win.UltraWinDock;
using Infragistics.Win.Misc;

namespace dashstatistic
{
    delegate void p2f(object o);

    public partial class Dashboard : UserControl
    {
        DataClasses1DataContext db;
        public string ConnectionString;
        public Dashboard(string ConnectionString)
        {
            InitializeComponent();
            this.ConnectionString = ConnectionString;
            Static.ConnectionString = ConnectionString;
            db = new DataClasses1DataContext(ConnectionString);
            var stat = db.STATISTIC_TYPEs.OrderBy(x=>x.INSERT_DATE).ToList();
            add_controls(stat);
            Static.MAIN_DASHBOARD = this;

        }

     
        void add_controls(object o)
        {
            List<STATISTIC_TYPE> stat = (List<STATISTIC_TYPE>)o;
            ultraTilePanel1.MinimumColumns = stat.Max(xx => xx.LOCATION_X) < 3 ? 3 : stat.Max(xx => xx.LOCATION_X);
            ultraTilePanel1.MinimumRows = stat.Max(xx => xx.LOCATION_Y) < 3 ? 3 : stat.Max(xx => xx.LOCATION_Y);

  
            foreach (var item in stat)
            {
                if (item.IS_DELETED) continue;
                UltraTile tile = new UltraTile();
                
                //tile.SetPositionInNormalMode(new Point(item.STATISTIC_TYPE_CODE), true);
                if(!item.LOCATION_AUTO && item.LOCATION_X!=0 && item.LOCATION_Y!=0)
                    tile.PositionInNormalMode = new Point(item.LOCATION_X-1, item.LOCATION_Y-1);
                tile.Control = new statisticflow(item, this);
                ultraTilePanel1.Tiles.Add(tile);
            }
            ultraTilePanel1.Visible = true;



            foreach (var item in db.TASKs.ToList())
            {
                if (item.APPOINTMENTs.Count>0)
                ultraTilePanel1.Controls.Add(new singleitemControl(item));
            }
        }
        public void Refresh()
        {
            p2f p = new p2f(add_controls);
            ultraTilePanel1.Visible = false;
            ultraTilePanel1.Tiles.Clear();
            Thread t=new Thread(delegate(object dash){
                Thread.Sleep(500);
                Dashboard main = (Dashboard)((object[])dash)[0];
                p2f pp = (p2f)((object[])dash)[1];
                db = new DataClasses1DataContext(ConnectionString);
                var stat = db.STATISTIC_TYPEs.OrderBy(x => x.INSERT_DATE).ToList();
                main.Invoke(pp, stat);
                });
                t.Start(new Object[]{this,p});
           

        
        }

       

     
    }
}
