﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace dashstatistic
{
    public partial class statisticflow : UserControl
    {

        STATISTIC_TYPE stat;
        Control ctl;
        public statisticflow(STATISTIC_TYPE stat,Control parent=null)
        {
            InitializeComponent();
            this.stat = stat;


            
            add_controls(stat);
            //lbltitle.Text = stat.STATISTIC_TYPE_NAME;
            //lbltitle.ForeColor = System.Drawing.ColorTranslator.FromHtml(stat.BODY_COLOR); 
            //var cvt = new FontConverter();


            //lbltitle.Font = cvt.ConvertFromString(stat.BODY_FONT) as Font;
            //ctl.Dock = DockStyle.Fill;
            //panelcontent.Controls.Add(ctl);
        }

        void add_controls(object o)
        {
            panelcontent.Controls.Clear();

            STATISTIC_TYPE sta = (STATISTIC_TYPE)o;
            if (stat.STATISTIC_TYPE_CODE == 0)
                this.ctl = new stringnotes((List<DATA_KEYPAIR_NOTE>)sta.DATA_KEYPAIR_NOTEs.ToList());
            else if (stat.STATISTIC_TYPE_CODE == 1)
                this.ctl = new keyvalue((List<DATA_KEYPAIR_NOTE>)sta.DATA_KEYPAIR_NOTEs.ToList());
            else if (stat.STATISTIC_TYPE_CODE == 2)
                this.ctl = new chart((List<DATA_KEYPAIR_NOTE>)sta.DATA_KEYPAIR_NOTEs.ToList(), sta.STATISTIC_TYPE_NAME);

            lbltitle.Text = sta.STATISTIC_TYPE_NAME;
            var cvt = new FontConverter();

            panel1.BackColor = System.Drawing.ColorTranslator.FromHtml(sta.TITLE_BACKCOLOR);
            lbltitle.ForeColor = System.Drawing.ColorTranslator.FromHtml(sta.TITLE_COLOR);
            lbltitle.Font = cvt.ConvertFromString(sta.TITLE_FONT) as Font;
            ctl.Dock = DockStyle.Fill;
            panelcontent.Controls.Add(ctl);

            panelcontent.Visible = true;
         
        }
  
   

        private void btnrefresh_Click(object sender, EventArgs e)
        {
            p2f p = new p2f(add_controls);

            panelcontent.Visible = false;

            Thread t = new Thread(delegate(object dash)
            {
                Thread.Sleep(500);
                statisticflow main = (statisticflow)((object[])dash)[0];
                p2f pp = (p2f)((object[])dash)[1];
                STATISTIC_TYPE sta = (STATISTIC_TYPE)((object[])dash)[2];
                //db = new DataClasses1DataContext(ConnectionString);
                //var stat = db.STATISTIC_TYPEs.OrderBy(x => x.INSERT_DATE).ToList();

                main.Invoke(pp, sta);

            });
            t.Start(new Object[] { this, p,this.stat });
        }

        private void btnsetting_Click(object sender, EventArgs e)
        {
            DashCustomize d = new DashCustomize(Static.ConnectionString, stat);
            d.ShowDialog();
            if (d.Current == null)
            {
                
                Static.MAIN_DASHBOARD.Invoke(new MethodInvoker(delegate() { Static.MAIN_DASHBOARD.Refresh(); 
                }));

            }
            else
            {

                this.stat = d.Current;
                btnrefresh_Click(sender, e);
            
            }
            
        }
       
    }
}
