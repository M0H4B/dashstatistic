﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class keyvalue : UserControl
    {
        public keyvalue(List<DATA_KEYPAIR_NOTE> data)
        {
            InitializeComponent();
            foreach (var item in data)
            {
                Control ctl = new keyvalueitemControl(item);
                ctl.Dock = DockStyle.Top;
                this.panel1.Controls.Add(ctl);
            }
        }
    }
}
