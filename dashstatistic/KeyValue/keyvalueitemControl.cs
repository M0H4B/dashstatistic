﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class keyvalueitemControl : UserControl
    {
        public keyvalueitemControl(DATA_KEYPAIR_NOTE key_val)
        {
            InitializeComponent();


            lblkey.ForeColor = System.Drawing.ColorTranslator.FromHtml(key_val.STATISTIC_TYPE.BODY_COLOR);
            var cvt = new FontConverter();
            lblkey.Font = cvt.ConvertFromString(key_val.STATISTIC_TYPE.BODY_FONT) as Font;

            lblvalue.ForeColor = System.Drawing.ColorTranslator.FromHtml(key_val.STATISTIC_TYPE.BODY_COLOR);
            lblvalue.Font = cvt.ConvertFromString(key_val.STATISTIC_TYPE.BODY_FONT) as Font;

            this.lblkey.Text = key_val.DATA_KEY;

            if (key_val.DATA_VALUE.Trim().ToUpper().StartsWith("SELECT"))
                this.lblvalue.Text = Static.Execute(key_val.DATA_VALUE.Trim());
            else
                this.lblvalue.Text = key_val.DATA_VALUE;
        }
    }
}
