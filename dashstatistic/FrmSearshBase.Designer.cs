﻿namespace dashstatistic
{
    partial class FrmSelectBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            GControls.UI.ManiXButton.Office2010Green office2010Green2 = new GControls.UI.ManiXButton.Office2010Green();
            GControls.UI.ManiXButton.Office2010Red office2010Red2 = new GControls.UI.ManiXButton.Office2010Red();
            this.PTop = new Stimulsoft.Controls.Win.DotNetBar.PanelEx();
            this.TSearsh = new Stimulsoft.Controls.Win.DotNetBar.Controls.TextBoxX();
            this.CBField = new Stimulsoft.Controls.Win.DotNetBar.Controls.ComboBoxEx();
            this.combobindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PBotton = new Stimulsoft.Controls.Win.DotNetBar.PanelEx();
            this.BtnSelect = new GControls.UI.ManiXButton.XButton();
            this.BtnCancel = new GControls.UI.ManiXButton.XButton();
            this.PCenter = new Stimulsoft.Controls.Win.DotNetBar.PanelEx();
            this.Grid = new GControls.UI.Grid.GGrid();
            this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iNSERTDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSDELETEDDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.lOCATIONXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lOCATIONYDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lOCATIONAUTODataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.combobindingSource)).BeginInit();
            this.PBotton.SuspendLayout();
            this.PCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // PTop
            // 
            this.PTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.PTop.ColorSchemeStyle = Stimulsoft.Controls.Win.DotNetBar.eDotNetBarStyle.Office2007;
            this.PTop.Controls.Add(this.TSearsh);
            this.PTop.Controls.Add(this.CBField);
            this.PTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PTop.Location = new System.Drawing.Point(0, 0);
            this.PTop.Name = "PTop";
            this.PTop.Size = new System.Drawing.Size(569, 58);
            this.PTop.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PTop.Style.BackColor1.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground;
            this.PTop.Style.BackColor2.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PTop.Style.Border = Stimulsoft.Controls.Win.DotNetBar.eBorderType.SingleLine;
            this.PTop.Style.BorderColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBorder;
            this.PTop.Style.ForeColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelText;
            this.PTop.Style.GradientAngle = 90;
            this.PTop.TabIndex = 0;
            // 
            // TSearsh
            // 
            this.TSearsh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // 
            // 
            this.TSearsh.Border.Class = "TextBoxBorder";
            this.TSearsh.Border.CornerType = Stimulsoft.Controls.Win.DotNetBar.eCornerType.Square;
            this.TSearsh.Font = new System.Drawing.Font("Tahoma", 13F);
            this.TSearsh.Location = new System.Drawing.Point(177, 12);
            this.TSearsh.Name = "TSearsh";
            this.TSearsh.Size = new System.Drawing.Size(380, 28);
            this.TSearsh.TabIndex = 1;
            this.TSearsh.TextChanged += new System.EventHandler(this.TSearsh_TextChanged);
            this.TSearsh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextSearch_KeyDown);
            this.TSearsh.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextSearch_KeyPress);
            // 
            // CBField
            // 
            this.CBField.DataSource = this.combobindingSource;
            this.CBField.DisplayMember = "name";
            this.CBField.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.CBField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBField.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CBField.Font = new System.Drawing.Font("Tahoma", 15F);
            this.CBField.FormattingEnabled = true;
            this.CBField.ItemHeight = 26;
            this.CBField.Location = new System.Drawing.Point(12, 12);
            this.CBField.Name = "CBField";
            this.CBField.Size = new System.Drawing.Size(148, 32);
            this.CBField.TabIndex = 2;
            this.CBField.ValueMember = "id";
            // 
            // combobindingSource
            // 
            this.combobindingSource.DataSource = typeof(dashstatistic.type);
            // 
            // PBotton
            // 
            this.PBotton.CanvasColor = System.Drawing.SystemColors.Control;
            this.PBotton.ColorSchemeStyle = Stimulsoft.Controls.Win.DotNetBar.eDotNetBarStyle.Office2007;
            this.PBotton.Controls.Add(this.BtnSelect);
            this.PBotton.Controls.Add(this.BtnCancel);
            this.PBotton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PBotton.Location = new System.Drawing.Point(0, 292);
            this.PBotton.Name = "PBotton";
            this.PBotton.Size = new System.Drawing.Size(569, 72);
            this.PBotton.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PBotton.Style.BackColor1.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground;
            this.PBotton.Style.BackColor2.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PBotton.Style.Border = Stimulsoft.Controls.Win.DotNetBar.eBorderType.SingleLine;
            this.PBotton.Style.BorderColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBorder;
            this.PBotton.Style.ForeColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelText;
            this.PBotton.Style.GradientAngle = 90;
            this.PBotton.TabIndex = 1;
            // 
            // BtnSelect
            // 
            this.BtnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            office2010Green2.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Green2.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Green2.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Green2.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Green2.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            office2010Green2.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            office2010Green2.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(126)))), ((int)(((byte)(43)))));
            office2010Green2.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(184)))), ((int)(((byte)(67)))));
            office2010Green2.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(126)))), ((int)(((byte)(43)))));
            office2010Green2.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(184)))), ((int)(((byte)(67)))));
            office2010Green2.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Green2.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Green2.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            office2010Green2.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            office2010Green2.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Green2.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Green2.TextColor = System.Drawing.Color.White;
            this.BtnSelect.ColorTable = office2010Green2;
            this.BtnSelect.Location = new System.Drawing.Point(427, 14);
            this.BtnSelect.Name = "BtnSelect";
            this.BtnSelect.Size = new System.Drawing.Size(130, 46);
            this.BtnSelect.TabIndex = 25;
            this.BtnSelect.Text = "Select";
            this.BtnSelect.Theme = GControls.UI.ManiXButton.Theme.MSOffice2010_Green;
            this.BtnSelect.UseVisualStyleBackColor = true;
            this.BtnSelect.Click += new System.EventHandler(this.BtnSelect_Click);
            // 
            // BtnCancel
            // 
            office2010Red2.BorderColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(72)))), ((int)(((byte)(161)))));
            office2010Red2.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(135)))), ((int)(((byte)(228)))));
            office2010Red2.ButtonMouseOverColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Red2.ButtonMouseOverColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Red2.ButtonMouseOverColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(225)))), ((int)(((byte)(137)))));
            office2010Red2.ButtonMouseOverColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(249)))), ((int)(((byte)(224)))));
            office2010Red2.ButtonNormalColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(77)))), ((int)(((byte)(45)))));
            office2010Red2.ButtonNormalColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(148)))), ((int)(((byte)(64)))));
            office2010Red2.ButtonNormalColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(77)))), ((int)(((byte)(45)))));
            office2010Red2.ButtonNormalColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(148)))), ((int)(((byte)(64)))));
            office2010Red2.ButtonSelectedColor1 = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(199)))), ((int)(((byte)(87)))));
            office2010Red2.ButtonSelectedColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(243)))), ((int)(((byte)(215)))));
            office2010Red2.ButtonSelectedColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(229)))), ((int)(((byte)(117)))));
            office2010Red2.ButtonSelectedColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(216)))), ((int)(((byte)(107)))));
            office2010Red2.HoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Red2.SelectedTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            office2010Red2.TextColor = System.Drawing.Color.White;
            this.BtnCancel.ColorTable = office2010Red2;
            this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnCancel.Location = new System.Drawing.Point(12, 14);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(130, 46);
            this.BtnCancel.TabIndex = 24;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.Theme = GControls.UI.ManiXButton.Theme.MSOffice2010_RED;
            this.BtnCancel.UseVisualStyleBackColor = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // PCenter
            // 
            this.PCenter.CanvasColor = System.Drawing.SystemColors.Control;
            this.PCenter.ColorSchemeStyle = Stimulsoft.Controls.Win.DotNetBar.eDotNetBarStyle.Office2007;
            this.PCenter.Controls.Add(this.Grid);
            this.PCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PCenter.Location = new System.Drawing.Point(0, 58);
            this.PCenter.Name = "PCenter";
            this.PCenter.Size = new System.Drawing.Size(569, 234);
            this.PCenter.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PCenter.Style.BackColor1.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground;
            this.PCenter.Style.BackColor2.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PCenter.Style.Border = Stimulsoft.Controls.Win.DotNetBar.eBorderType.SingleLine;
            this.PCenter.Style.BorderColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBorder;
            this.PCenter.Style.ForeColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelText;
            this.PCenter.Style.GradientAngle = 90;
            this.PCenter.TabIndex = 2;
            // 
            // Grid
            // 
            this.Grid.AllowUserToAddRows = false;
            this.Grid.AllowUserToDeleteRows = false;
            this.Grid.AutoGenerateColumns = false;
            this.Grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn,
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn,
            this.iNSERTDATEDataGridViewTextBoxColumn,
            this.iSDELETEDDataGridViewCheckBoxColumn,
            this.lOCATIONXDataGridViewTextBoxColumn,
            this.lOCATIONYDataGridViewTextBoxColumn,
            this.lOCATIONAUTODataGridViewCheckBoxColumn});
            this.Grid.DataSource = this.bindingSource;
            this.Grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grid.FirstColor = System.Drawing.Color.White;
            this.Grid.Location = new System.Drawing.Point(0, 0);
            this.Grid.MultiSelect = false;
            this.Grid.Name = "Grid";
            this.Grid.OpenSettingInMenu = true;
            this.Grid.RCount = -1;
            this.Grid.ReadOnly = true;
            this.Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grid.Size = new System.Drawing.Size(569, 234);
            this.Grid.TabIndex = 3;
            this.Grid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellContentClick);
            this.Grid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid_CellDoubleClick);
            this.Grid.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Grid_MouseClick);
            // 
            // bindingSource
            // 
            this.bindingSource.AllowNew = false;
            this.bindingSource.DataSource = typeof(dashstatistic.STATISTIC_TYPE);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // sTATISTICTYPENAMEDataGridViewTextBoxColumn
            // 
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn.DataPropertyName = "STATISTIC_TYPE_NAME";
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn.HeaderText = "Name";
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn.Name = "sTATISTICTYPENAMEDataGridViewTextBoxColumn";
            this.sTATISTICTYPENAMEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sTATISTICTYPECODEDataGridViewTextBoxColumn
            // 
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn.DataPropertyName = "STATISTIC_TYPE_CODE";
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn.HeaderText = "Code";
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn.Name = "sTATISTICTYPECODEDataGridViewTextBoxColumn";
            this.sTATISTICTYPECODEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iNSERTDATEDataGridViewTextBoxColumn
            // 
            this.iNSERTDATEDataGridViewTextBoxColumn.DataPropertyName = "INSERT_DATE";
            this.iNSERTDATEDataGridViewTextBoxColumn.HeaderText = "INSERT_DATE";
            this.iNSERTDATEDataGridViewTextBoxColumn.Name = "iNSERTDATEDataGridViewTextBoxColumn";
            this.iNSERTDATEDataGridViewTextBoxColumn.ReadOnly = true;
            this.iNSERTDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // iSDELETEDDataGridViewCheckBoxColumn
            // 
            this.iSDELETEDDataGridViewCheckBoxColumn.DataPropertyName = "IS_DELETED";
            this.iSDELETEDDataGridViewCheckBoxColumn.HeaderText = "IS_DELETED";
            this.iSDELETEDDataGridViewCheckBoxColumn.Name = "iSDELETEDDataGridViewCheckBoxColumn";
            this.iSDELETEDDataGridViewCheckBoxColumn.ReadOnly = true;
            this.iSDELETEDDataGridViewCheckBoxColumn.Visible = false;
            // 
            // lOCATIONXDataGridViewTextBoxColumn
            // 
            this.lOCATIONXDataGridViewTextBoxColumn.DataPropertyName = "LOCATION_X";
            this.lOCATIONXDataGridViewTextBoxColumn.HeaderText = "LOCATION_X";
            this.lOCATIONXDataGridViewTextBoxColumn.Name = "lOCATIONXDataGridViewTextBoxColumn";
            this.lOCATIONXDataGridViewTextBoxColumn.ReadOnly = true;
            this.lOCATIONXDataGridViewTextBoxColumn.Visible = false;
            // 
            // lOCATIONYDataGridViewTextBoxColumn
            // 
            this.lOCATIONYDataGridViewTextBoxColumn.DataPropertyName = "LOCATION_Y";
            this.lOCATIONYDataGridViewTextBoxColumn.HeaderText = "LOCATION_Y";
            this.lOCATIONYDataGridViewTextBoxColumn.Name = "lOCATIONYDataGridViewTextBoxColumn";
            this.lOCATIONYDataGridViewTextBoxColumn.ReadOnly = true;
            this.lOCATIONYDataGridViewTextBoxColumn.Visible = false;
            // 
            // lOCATIONAUTODataGridViewCheckBoxColumn
            // 
            this.lOCATIONAUTODataGridViewCheckBoxColumn.DataPropertyName = "LOCATION_AUTO";
            this.lOCATIONAUTODataGridViewCheckBoxColumn.HeaderText = "LOCATION_AUTO";
            this.lOCATIONAUTODataGridViewCheckBoxColumn.Name = "lOCATIONAUTODataGridViewCheckBoxColumn";
            this.lOCATIONAUTODataGridViewCheckBoxColumn.ReadOnly = true;
            this.lOCATIONAUTODataGridViewCheckBoxColumn.Visible = false;
            // 
            // FrmSelectBase
            // 
            this.AcceptButton = this.BtnSelect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.CancelButton = this.BtnCancel;
            this.ClientSize = new System.Drawing.Size(569, 364);
            this.Controls.Add(this.PCenter);
            this.Controls.Add(this.PBotton);
            this.Controls.Add(this.PTop);
            this.Name = "FrmSelectBase";
            this.Text = "";
            this.PTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.combobindingSource)).EndInit();
            this.PBotton.ResumeLayout(false);
            this.PCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Stimulsoft.Controls.Win.DotNetBar.PanelEx PTop;
        private Stimulsoft.Controls.Win.DotNetBar.PanelEx PBotton;
        private Stimulsoft.Controls.Win.DotNetBar.PanelEx PCenter;
        private Stimulsoft.Controls.Win.DotNetBar.Controls.ComboBoxEx CBField;
        private GControls.UI.ManiXButton.XButton BtnSelect;
        private GControls.UI.ManiXButton.XButton BtnCancel;
        private Stimulsoft.Controls.Win.DotNetBar.Controls.TextBoxX TSearsh;
        private System.Windows.Forms.BindingSource bindingSource;
        public GControls.UI.Grid.GGrid Grid;
        private System.Windows.Forms.BindingSource combobindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTATISTICTYPENAMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTATISTICTYPECODEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iNSERTDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn iSDELETEDDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lOCATIONXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lOCATIONYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn lOCATIONAUTODataGridViewCheckBoxColumn;
    }
}
