﻿using dashstatistic;
using GBase;
using GBase.Class;
using GControls.Class;
using GControls.Message;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class FrmSelectBase : FrmBase
    {
        DataClasses1DataContext db;

        public FrmSelectBase(string ConnectionString)
        {
            InitializeComponent();
            db = new DataClasses1DataContext(ConnectionString);
            bindingSource.DataSource = db.STATISTIC_TYPEs.AsQueryable();
            combobindingSource.Add(new type() { id = 0, name = "ALL" });
            combobindingSource.Add(new type() { id = 1, name = "Code" });
            combobindingSource.Add(new type() { id = 2, name = "Name" });

            Grid.Columns[0].Visible = false;
        }

       

        private void TSearsh_TextChanged(object sender, EventArgs e)
        {
            if (TSearsh.Text.Trim() == string.Empty)
            {
                bindingSource.DataSource = db.STATISTIC_TYPEs.AsQueryable();
                return;            
            }
            bindingSource.Filter = "STATISTIC_TYPE_CODE='" + TSearsh.Text + "'";
            if (CBField.SelectedValue.ToString()=="1")
                bindingSource.DataSource = db.STATISTIC_TYPEs.Where(x => x.STATISTIC_TYPE_CODE.ToString() == TSearsh.Text);
            else if (CBField.SelectedValue.ToString()=="2")
                bindingSource.DataSource = db.STATISTIC_TYPEs.Where(x => x.STATISTIC_TYPE_NAME.ToString() == TSearsh.Text);

            else
                bindingSource.DataSource = db.STATISTIC_TYPEs.Where(x => x.STATISTIC_TYPE_CODE.ToString() == TSearsh.Text || x.STATISTIC_TYPE_NAME.ToString() == TSearsh.Text);

        }

     

        private void TextSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (Grid.CurrentRow != null & Grid.SelectedRows.Count > 0)
                    Grid.CurrentRow.Selected = true;
               // if (Grid.SelectedRows.Count > 0)
                    //BOK_Click(sender, e);
            }
        }

        private void TextSearch_KeyDown(object sender, KeyEventArgs e)
        {
            int rowsDisp = Grid.DisplayedRowCount(false);

            int index = 0;
            if (Grid.CurrentCell != null)
            {
                index = Grid.CurrentCell.RowIndex;

                if (e.KeyCode.Equals(Keys.Up))
                {
                    if (index > 0)
                    {
                        if (e.Shift & Grid.MultiSelect)
                        {
                            DataGridViewSelectedRowCollection SelectRow = Grid.SelectedRows;

                            Grid.CurrentCell = Grid.Rows[index - 1].Cells[1];

                            foreach (DataGridViewRow item in SelectRow)
                                item.Selected = true;

                            Grid.Rows[index - 1].Selected = true;
                        }
                        else
                            Grid.CurrentCell = Grid.Rows[index - 1].Cells[1];
                    }
                }
                else if (e.KeyCode.Equals(Keys.Down))
                {
                    if (index != Grid.Rows.Count - 1)
                    {
                        if (e.Shift & Grid.MultiSelect)
                        {
                            DataGridViewSelectedRowCollection SelectRow = Grid.SelectedRows;

                            Grid.CurrentCell = Grid.Rows[index + 1].Cells[1];

                            foreach (DataGridViewRow item in SelectRow)
                                item.Selected = true;

                            Grid.Rows[index + 1].Selected = true;
                        }
                        else
                            Grid.CurrentCell = Grid.Rows[index + 1].Cells[1];
                    }
                }
                else if (e.KeyCode.Equals(Keys.End))
                {
                    if (index != Grid.Rows.Count - 1)
                    {
                        if (e.Shift & Grid.MultiSelect)
                        {
                            DataGridViewSelectedRowCollection SelectRow = Grid.SelectedRows;
                            Grid.CurrentCell = Grid.Rows[Grid.Rows.Count - 1].Cells[1];

                            for (int i = index; i < Grid.Rows.Count; i++)
                                Grid.Rows[i].Selected = true;


                            foreach (DataGridViewRow item in SelectRow)
                                item.Selected = true;
                        }
                        else
                            Grid.CurrentCell = Grid.Rows[Grid.Rows.Count - 1].Cells[1];
                    }

                }
                else if (e.KeyCode.Equals(Keys.PageUp))
                {
                    if (index > 0)
                    {
                        if (e.Shift & Grid.MultiSelect)
                        {
                            DataGridViewSelectedRowCollection SelectRow = Grid.SelectedRows;
                            int y = index - rowsDisp;
                            if (y < 0)
                                y = 0;
                            Grid.CurrentCell = Grid.Rows[y].Cells[1];

                            for (int i = index; i > y; i--)
                                Grid.Rows[i].Selected = true;

                            foreach (DataGridViewRow item in SelectRow)
                                item.Selected = true;
                        }
                        else
                        {
                            int i = index - rowsDisp;
                            if (i < 0)
                                i = 0;
                            Grid.CurrentCell = Grid.Rows[i].Cells[1];
                        }
                    }


                }
                else if (e.KeyCode.Equals(Keys.PageDown))
                {
                    if (index != Grid.Rows.Count - 1)
                    {
                        if (e.Shift & Grid.MultiSelect)
                        {
                            DataGridViewSelectedRowCollection SelectRow = Grid.SelectedRows;
                            int y = index + rowsDisp;
                            if (y >= Grid.Rows.Count - 1)
                                y = Grid.Rows.Count - 1;

                            Grid.CurrentCell = Grid.Rows[y].Cells[1];

                            for (int i = index; i < y; i++)
                                Grid.Rows[i].Selected = true;


                            foreach (DataGridViewRow item in SelectRow)
                                item.Selected = true;
                        }
                        else
                        {
                            int i = index + rowsDisp;
                            if (i >= Grid.Rows.Count - 1)
                                i = Grid.Rows.Count - 1;
                            Grid.CurrentCell = Grid.Rows[i].Cells[1];
                        }
                    }

                }
                else if (e.KeyCode.Equals(Keys.Home))
                {
                    if (index > 0)
                    {
                        if (e.Shift & Grid.MultiSelect)
                        {
                            DataGridViewSelectedRowCollection SelectRow = Grid.SelectedRows;
                            Grid.CurrentCell = Grid.Rows[0].Cells[1];

                            for (int i = index; i > 0; i--)
                                Grid.Rows[i].Selected = true;

                            foreach (DataGridViewRow item in SelectRow)
                                item.Selected = true;
                        }
                        else
                            Grid.CurrentCell = Grid.Rows[0].Cells[1];
                    }


                }



            }
        }


        public STATISTIC_TYPE Current;
        private void BtnSelect_Click(object sender, EventArgs e)
        {
            Current = (STATISTIC_TYPE)bindingSource.Current;
           this.DialogResult= DialogResult.OK;
           this.Close();

        }

        private void Grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Current = (STATISTIC_TYPE)bindingSource.Current;

        }

      

        private void Grid_MouseClick(object sender, MouseEventArgs e)
        {
            TSearsh.Focus();
        }

        private void Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Current = null;
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

    }
}
