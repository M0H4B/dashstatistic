﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dashstatistic
{
    public partial class TestQuery : Form
    {
        public string qeury="";
        public TestQuery(string value)
        {
            InitializeComponent();
            this.qeury = value;
            this.richTextBox1.Text = value;
        }

        private void btntest_Click(object sender, EventArgs e)
        {
            lblresult.Text=Static.Execute(richTextBox1.Text,true);
        }

        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                lblresult.Text = Static.Execute(richTextBox1.Text, true);

            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            lblresult.Text = "";
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.qeury = richTextBox1.Text;
        }
    }
}
