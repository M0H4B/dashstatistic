﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace dashstatistic
{
    class Static
    {
        public static string ConnectionString = @"Data Source=root-pc\sqlexpress;Initial Catalog=statistics;Integrated Security=True";
        public static STATISTIC_TYPE LAST_Value = new STATISTIC_TYPE();
        public static Dashboard MAIN_DASHBOARD;
        public static string Execute(string sql,bool showerror=false) { 
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = new SqlConnection(ConnectionString);
                con.Open();
                cmd = new SqlCommand(sql);
                cmd.Connection = con;
                var obj = cmd.ExecuteScalar();
                return obj.ToString();
            }
            catch (Exception why){
                if (showerror)
                    return why.Message;
                else
                    return "Error";
            }
            finally { con.Close(); }
        
        }
    }
}
