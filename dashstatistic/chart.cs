﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace dashstatistic
{
    public partial class chart : UserControl
    {
        Thread t;
        bool start = true;
        public chart(List<DATA_KEYPAIR_NOTE> data,string series)
        {
            InitializeComponent();
            chart1.Series.Add("Salary");
            chart1.Titles.Add("Salary Chart");

            chart1.Series.Add(series);
            chart1.Titles.Add(series + "Chart");
            foreach (var item in data)
            {
                chart1.Series[series].Points.AddXY(item.DATA_KEY, (item.DATA_VALUE.ToString()));
                
            }

             t = new Thread(delegate(object o){
                object[] obj = (object[])o;
                chart c = (chart)obj[0];
                while (true)
                {
                    Thread.Sleep(9000);

                    if (start)
                    {
                        try
                        {
                            c.Invoke(new MethodInvoker(delegate()
                            {
                                Random r = new Random();
                                chart1.Series["Salary"].Points.AddXY("Ajay", (r.Next(300, 1000).ToString()));

                            }));
                        }
                        catch { break; }
                    }
                }
            });

             Thread.Sleep(1000);

            t.Start(new object[1]{this});
        }

        private void button1_Click(object sender, EventArgs e)
        {
            start = !start;
        }

       
       
    }
}
