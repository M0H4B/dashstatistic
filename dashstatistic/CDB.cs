﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
//using Microsoft.SqlServer.Management.Smo;
//using Microsoft.SqlServer.Management.Common;
using System.Xml.Serialization;
using GControls;
using System.Data;
using System.Windows.Forms;

namespace GBase.Class
{
    public class DBSqlServer
    {

        public static bool TestConn()
        {
            bool Result = false;
            if (CBase.Conn.State == System.Data.ConnectionState.Open)
            {
                Result = true;
            }
            else if (CBase.Conn.State == System.Data.ConnectionState.Closed)
            {
                try
                {
                    CBase.Conn.Open();
                    Result = true;

                }
                catch (Exception ex)
                {
                    GControls.Message.MSBox.Show("Erro", ex);

                    Result = false;
                }
            }
            return Result;
        }

        public static void CloseConn()
        {
            if (CBase.Conn.State == System.Data.ConnectionState.Open)
                CBase.Conn.Close();
        }

        public static bool ExecuteFiles(string Path)
        {
            bool Result = true;
            string script = File.ReadAllText(Path);

            SqlConnection conn = new SqlConnection(CBase.ConnectionString);
            try
            {
                SqlCommand Comm = conn.CreateCommand();
                Comm.CommandText = script;
                conn.Open();
                //Comm.ExecuteNonQuery();
                //conn.Close();
                /*
                Server server = new Server(new ServerConnection(conn));
                server.ConnectionContext.ExecuteNonQuery(script);
                */
                MessageBox.Show("this functionality is not supported in this version");
            }
            catch (Exception ex)
            {
                Result = false;
                GControls.Message.MSBox.Show("Erro", ex);
            }
            finally { conn.Close(); }
            return Result;
        }

        public static object ExecuteScalar(string Sql)
        {
            object Result = null;
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandText = Sql;
                Result = Comm.ExecuteScalar();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }
            if (Result == null)
                Result = "";

            return Result;
        }

        public static SqlDataReader ExecuteReader(string Sql)
        {
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandText = Sql;
                return Comm.ExecuteReader();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }
            return null;
        }

        public static List<object> GetList(string Sql)
        {
            List<object> Result = new List<object>();
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandText = Sql;
                SqlDataReader dr = Comm.ExecuteReader();
                while (dr.Read())
                    Result.Add(dr[0]);
                dr.Close();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }
            return Result;
        }

        public static List<object> GetList(string TableName, string Columns)
        {
            return GetList("prcGetListDistinct " + TableName + "," + Columns);
        }


        public static void ExecuteNonQuery(string Sql)
        {
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandText = Sql;
                Comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }
        }

        public static DataTable GetTheDataTable(string Sql)
        {
            DataTable dtDataTablesList = new DataTable();
            SqlConnection spContentConn = new SqlConnection(CBase.ConnectionString);
            try
            {
                spContentConn.Open();
                SqlCommand sqlCmd = new SqlCommand(Sql, spContentConn);
                sqlCmd.CommandTimeout = 0;
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.ExecuteNonQuery();
                SqlDataAdapter adptr = new SqlDataAdapter(sqlCmd);
                adptr.Fill(dtDataTablesList);
                spContentConn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (spContentConn != null)
                    spContentConn.Dispose();
            }
            return dtDataTablesList;
        }

        public static CNaveget GetNaveget(string TableName, int dir, int g, bool FirstAndLastOnly, int CurNumber, string WhereNaveget)
        {
            CNaveget Result = new CNaveget();
            SqlCommand Comm = new SqlCommand("prcNavigate1", CBase.Conn);
            Comm.CommandType = CommandType.StoredProcedure;
            Comm.Parameters.AddWithValue("@tbl", TableName);
            Comm.Parameters.AddWithValue("@dir", dir);
            Comm.Parameters.AddWithValue("@g", g);
            Comm.Parameters.AddWithValue("@FldNum", "Number");
            Comm.Parameters.AddWithValue("@bFirstAndLastOnly", FirstAndLastOnly);
            Comm.Parameters.AddWithValue("@CurFldNumber", CurNumber);
            Comm.Parameters.AddWithValue("@WhereNaveget", WhereNaveget);
            SqlDataReader Dr;
            try
            {
                Dr = Comm.ExecuteReader();
                Dr.Read();
                if (Dr.HasRows)
                {
                    Result.EndGUID = Dr["LastGUID"];
                    Result.GUID = Dr["GUID"];
                    Result.FirstGUID = Dr["FirstGUID"];
                }

                Dr.Close();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Error", ex);
            }
            return Result;
        }

        public static string GetExtendedProperties(string Key)
        {
            string Result = "";
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandText = "select value from sys.extended_properties where name = '" + Key + "'";
                Result = Comm.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                Result = "";
            }
            return Result;
        }

        public static void SetExtendedProperties(string Key, string Value)
        {
            string sql = "EXEC Prc_SetExtendedProperties '" + Key + "', '" + Value + "'";
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandText = sql;
                Comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }
        }

        public static int GetNextNumber(string TableName, string Columns, string Filter)
        {
            int Result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("prcGetNextNumber", CBase.Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TableName", TableName);
                cmd.Parameters.AddWithValue("@FldName", Columns);
                cmd.Parameters.AddWithValue("@Filter", Filter);
                Result = int.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
            }
            return Result;
        }

        public static List<string> GetDataBaseName()
        {
            List<string> Result = new List<string>();
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();

                Comm.CommandText = "SELECT name FROM master.dbo.sysdatabases";
                SqlDataReader dr = Comm.ExecuteReader();
                while (dr.Read())
                {
                    Result.Add(dr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }

            return Result;
        }

        //-------------------------------------------------------------------------------------------------
        //salah
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stored_name"></param>
        /// <param name="p"></param>
        public static void ExecuteStoredProcedure(string stored_name, SqlParameter[] p)
        {
            SqlConnection conn = new SqlConnection(CBase.ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = stored_name;
            cmd.Connection = conn;
            if (p != null)
                cmd.Parameters.AddRange(p);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();
        }
        /// <summary>
        /// Execute Stored Procedure and Return Parameter value
        /// </summary>
        /// <param name="stored_name"> stored name</param>
        /// <param name="p">SqlParameter Array</param>
        /// <param name="IDindex">Parametar Index </param>
        /// <returns> Return Parameter value</returns>
        public static int ExecuteStoredProcedure(string stored_name, SqlParameter[] p, int IDindex)
        {
            SqlConnection conn = new SqlConnection(CBase.ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = stored_name;
            cmd.Connection = conn;
            if (p != null)
                cmd.Parameters.AddRange(p);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            conn.Dispose();

            return Convert.ToInt32(cmd.Parameters[IDindex].Value);
        }


        public static DataTable GetDataStoredProcedure(string stored_name, SqlParameter[] p)
        {
            SqlConnection conn = new SqlConnection(CBase.ConnectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = stored_name;
            cmd.Connection = conn;
            if (p != null)
                cmd.Parameters.AddRange(p);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        /// <summary>
        /// Get All ID of select table with Defult ID Column Name
        /// </summary>
        /// <param name="TableName">Pass table name</param>
        /// <returns>List of IDs</returns>
        public static List<int> GetAllID(string TableName)
        {
            List<int> Result = new List<int>();
            string Sql = "Select ID  from " + TableName;

            DataTable dt = DBSqlServer.GetTheDataTable(Sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Result.Add(Convert.ToInt32(dt.Rows[i][0]));
            }
            return Result;
        }
        /// <summary>
        /// Get All ID of select table with select ID Column Name
        /// </summary>
        /// <param name="TableName">Pass Table Name</param>
        /// <param name="IDColName">Pass ID Column Name </param>
        /// <returns>>List of IDs</returns>
        public static List<int> GetAllID(string TableName, string IDColName)
        {
            List<int> Result = new List<int>();
            string Sql = "Select " + IDColName + "  from " + TableName;

            DataTable dt = DBSqlServer.GetTheDataTable(Sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Result.Add(Convert.ToInt32(dt.Rows[i][0]));
            }
            return Result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sql"></param>
        /// <returns></returns>
        public static object ExecuteScalarStoredProcedure(string stored_name, SqlParameter[] p)
        {
            object Result = null;
            try
            {
                SqlCommand Comm = CBase.Conn.CreateCommand();
                Comm.CommandType = CommandType.StoredProcedure;
                Comm.CommandText = stored_name;
                if (p != null)
                    Comm.Parameters.AddRange(p);
                Result = Comm.ExecuteScalar();
            }
            catch (Exception ex)
            {
                GControls.Message.MSBox.Show("Sql Error ", ex);
            }
            if (Result == null)
                Result = "";

            return Result;
        }

    }

    public class CSqlConnection
    {
        public string Server { get; set; }
        public string DataBase { get; set; }
        public string UserID { get; set; }
        public string Password { get; set; }
        public bool IntegratedSecurity { get; set; }
        public int PacketSize { get; set; }
        public int ConnectTimeout { get; set; }
        public CSqlConnection() { }
        public CSqlConnection(SqlConnectionStringBuilder S)
        {
            this.Server = S.DataSource;
            this.DataBase = S.InitialCatalog;
            this.UserID = S.UserID;
            this.Password = S.Password;
            this.IntegratedSecurity = S.IntegratedSecurity;
            this.PacketSize = S.PacketSize;
            this.ConnectTimeout = S.ConnectTimeout;
        }

        public CSqlConnection(string ConnectionString)
            : this(new SqlConnectionStringBuilder(ConnectionString))
        {
        }
        public bool SaveXmlSerializer(string Path, out Exception e)
        {
            bool Result = true;
            e = null;
            try
            {
                XmlSerializer writer = new XmlSerializer(typeof(CSqlConnection));
                FileStream file = File.Create(Path);
                writer.Serialize(file, this);
                file.Close();
            }
            catch (Exception ex)
            {
                e = ex;
                Result = false;
            }
            return Result;
        }

        public static CSqlConnection LoadXmlSerializer(string Path, out Exception e)
        {
            CSqlConnection Result = null;
            e = null;
            try
            {
                XmlSerializer reader = new XmlSerializer(typeof(CSqlConnection));
                StreamReader file = new StreamReader(Path);
                Result = (CSqlConnection)reader.Deserialize(file);
                file.Close();
            }
            catch (Exception ex)
            {
                e = ex;
                Result = null;
            }
            return Result;
        }

        public static CSqlConnection LoadXmlSerializer(string Path)
        {
            Exception e;
            return LoadXmlSerializer(Path, out e);
        }

        public string GetSqlConnection()
        {
            return GetSqlConnectionStringBuilder().ConnectionString;
        }

        public SqlConnectionStringBuilder GetSqlConnectionStringBuilder()
        {
            SqlConnectionStringBuilder Result = new SqlConnectionStringBuilder();
            Result.DataSource = Server;
            Result.InitialCatalog = DataBase;
            Result.IntegratedSecurity = IntegratedSecurity;
            Result.UserID = UserID;
            Result.Password = Password;
            Result.PacketSize = PacketSize;
            Result.ConnectTimeout = ConnectTimeout;

            return Result;
        }
    }
}
