﻿namespace dashstatistic
{
    partial class Dashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.panel2 = new System.Windows.Forms.Panel();
            this.ultraTilePanel1 = new Infragistics.Win.Misc.UltraTilePanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTilePanel1)).BeginInit();
            this.ultraTilePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.ultraTilePanel1);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(724, 395);
            this.panel2.TabIndex = 2;
            // 
            // ultraTilePanel1
            // 
            this.ultraTilePanel1.AccessibleRole = System.Windows.Forms.AccessibleRole.Caret;
            this.ultraTilePanel1.AllowDrop = true;
            this.ultraTilePanel1.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Standard;
            this.ultraTilePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraTilePanel1.LargeTileOrientation = Infragistics.Win.Misc.TileOrientation.Vertical;
            this.ultraTilePanel1.LargeTilePosition = Infragistics.Win.Misc.LargeTilePosition.Top;
            this.ultraTilePanel1.Location = new System.Drawing.Point(0, 0);
            this.ultraTilePanel1.MaximumVisibleLargeTiles = 1;
            this.ultraTilePanel1.MinimumColumns = 3;
            this.ultraTilePanel1.MinimumRows = 2;
            this.ultraTilePanel1.MinimumSize = new System.Drawing.Size(700, 350);
            this.ultraTilePanel1.MinimumTileSize = new System.Drawing.Size(295, 185);
            this.ultraTilePanel1.Name = "ultraTilePanel1";
            this.ultraTilePanel1.NormalModeDimensions = new System.Drawing.Size(0, 0);
            this.ultraTilePanel1.Size = new System.Drawing.Size(724, 395);
            this.ultraTilePanel1.TabIndex = 0;
            appearance1.BackColor = System.Drawing.Color.CornflowerBlue;
            appearance1.ForeColor = System.Drawing.Color.White;
            this.ultraTilePanel1.TileSettings.HeaderAppearance = appearance1;
            this.ultraTilePanel1.TileSettings.HeaderExtent = 16;
            this.ultraTilePanel1.UseFlatMode = Infragistics.Win.DefaultableBoolean.True;
            this.ultraTilePanel1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(724, 395);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Name = "Dashboard";
            this.Size = new System.Drawing.Size(724, 395);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTilePanel1)).EndInit();
            this.ultraTilePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Infragistics.Win.Misc.UltraTilePanel ultraTilePanel1;
    }
}
