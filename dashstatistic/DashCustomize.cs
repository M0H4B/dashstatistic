﻿using GBase;
using GControls.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace dashstatistic
{
    public partial class DashCustomize : GBase.FrmBase
    {
        DataClasses1DataContext db;
        public string ConnectionString;
        public STATISTIC_TYPE stat;
        
        public DashCustomize(string ConnectionString,STATISTIC_TYPE stat=null)
        {
            InitializeComponent();


      


            BtnClose.Image = ImageListBase24.Images["false"];
            BtnNew.Image = ImageListBase24.Images["Card-add"];
            BtnSave.Image = ImageListBase24.Images["Card-Update"];
            BtnDel.Image = ImageListBase24.Images["Card-delete"];
            btnsubnew.Image = ImageListBase24.Images["Card-add"];
            btnsubdel.Image = ImageListBase24.Images["Card-delete"];
            BtnEnd.BackgroundImage = ImageListCard36.Images["end"];
            BtnNext.BackgroundImage = ImageListCard36.Images["right"];
            BtnLast.BackgroundImage = ImageListCard36.Images["left"];
            BtnFirst.BackgroundImage = ImageListCard36.Images["first"];

            colorDialog1.CustomColors = new int[]{
                ColorTranslator.ToOle(Color.CornflowerBlue),
                ColorTranslator.ToOle(Color.White)

            };
            this.ConnectionString = ConnectionString;
            Static.ConnectionString = ConnectionString;
            this.stat = stat;
            db = new DataClasses1DataContext(ConnectionString);
            if (stat == null)
            {
                sTATISTIC_TYPEsBindingSource.DataSource = db.STATISTIC_TYPEs.AsQueryable();

            }
            else
            {
                sTATISTIC_TYPEsBindingSource.DataSource = db.STATISTIC_TYPEs.AsQueryable().Where(x=>x.ID==stat.ID).First();

                sTATISTIC_TYPEsBindingSource.AllowNew = false;
                PTop.Enabled = false;
                BtnNew.Enabled = false;
                BtnSearch.Enabled = false;
                stat.PropertyChanging += (object esender, PropertyChangingEventArgs ee) =>
                {
                    _dataEdited = true;
                };
            }


            bindingSource1.Add(new type() { id = 0, name = "string" });
            bindingSource1.Add(new type() { id = 1, name = "keyvalue" });
            bindingSource1.Add(new type() { id = 2, name = "chart" });
            dATA_KEYPAIR_NOTEsDataGridView.Columns[0].Visible = false;
            
        }

      

        private void sTATISTIC_TYPEsBindingSource_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new STATISTIC_TYPE()
            {
                ID = Guid.NewGuid(),
                INSERT_DATE = DateTime.Now,
                IS_DELETED = false,
                BODY_FONT = "Segoe UI Symbol, 11.25pt",
                BODY_COLOR = "Red",
                TITLE_COLOR = "Black",
                TITLE_BACKCOLOR = "CornflowerBlue",
                TITLE_FONT = "Segoe UI Symbol, 11.25pt",
            };
        }

        private void dATA_KEYPAIR_NOTEsBindingSource_AddingNew_1(object sender, AddingNewEventArgs e)
        {
            var x = (STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current;
            e.NewObject = new DATA_KEYPAIR_NOTE()
            {
                ID = Guid.NewGuid(),
                STATISTIC_TYPE_ID=x.ID,
                INSERT_DATE = DateTime.Now,
                IS_DELETED = false,
                DATA_KEY = "",
                DATA_NOTE = "",
                DATA_VALUE = ""
            };

        }

       

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            tableLayoutPanel2.Enabled = !checkBox1.Checked;
        }


        bool _dataEdited = false;

        private void itemBindingSource_BindingComplete(object sender, BindingCompleteEventArgs e)
        {
            //if (e.BindingCompleteContext == BindingCompleteContext.DataSourceUpdate)
            //{

            //    if (!_dataEdited && (e.BindingCompleteState == BindingCompleteState.Success)
            //    )
            //    {
            //        _dataEdited = true;
            //    }

            //}

        }

  

        private void DashCustomize_FormClosing(object sender, FormClosingEventArgs e)
        {

            Current =(STATISTIC_TYPE) sTATISTIC_TYPEsBindingSource.Current;
            if (_dataEdited)
            {

                string msg = "Do you want to save changes to this record?";

                if (MessageBox.Show(msg, "Save Changes?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {

                    //btnSave_Click(sender, e);

                }

                else
                {

                    //itemBindingSource.CancelEdit();
                    e.Cancel = true;


                }

            }

            //reset the edited flag for next record

            _dataEdited = false;
        }

        private void sTATISTIC_TYPEsBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            if (sTATISTIC_TYPEsBindingSource.Count == 0) return;
            lab_Count.Text = sTATISTIC_TYPEsBindingSource.Count.ToString();
            TNumber.Text = (sTATISTIC_TYPEsBindingSource.Position+1).ToString();
            if (((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).STATISTIC_TYPE_CODE == 0)
            {
                dATA_KEYPAIR_NOTEsDataGridView.Columns[1].Visible = false;
                dATA_KEYPAIR_NOTEsDataGridView.Columns[2].Visible = false;
                dATA_KEYPAIR_NOTEsDataGridView.Columns[3].Visible = true;
            }
            else
            //if (((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).STATISTIC_TYPE_CODE == 1)
            {
                dATA_KEYPAIR_NOTEsDataGridView.Columns[1].Visible = true;
                dATA_KEYPAIR_NOTEsDataGridView.Columns[2].Visible = true;
                dATA_KEYPAIR_NOTEsDataGridView.Columns[3].Visible = false;
            }


            //do for color and font

            Color mycolor = System.Drawing.ColorTranslator.FromHtml(((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).BODY_COLOR);
            txtbdcolor.ForeColor = mycolor;

             mycolor = System.Drawing.ColorTranslator.FromHtml(((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).TITLE_COLOR);
            txtitleforecolor.ForeColor = mycolor;
             mycolor = System.Drawing.ColorTranslator.FromHtml(((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).TITLE_BACKCOLOR);
            txtitlebackcolor.ForeColor = mycolor;

            var cvt = new FontConverter();
            txtbdfont.Font = cvt.ConvertFromString(((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).BODY_FONT) as Font;
            txtbdfont.Font = cvt.ConvertFromString(((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).TITLE_FONT) as Font;


        }

        private void DashCustomize_Load(object sender, EventArgs e)
        {

        }

        private void BtnFirst_Click(object sender, EventArgs e)
        {
            sTATISTIC_TYPEsBindingSource.MoveFirst();
        }

        private void BtnLast_Click(object sender, EventArgs e)
        {
            sTATISTIC_TYPEsBindingSource.MovePrevious();

        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            sTATISTIC_TYPEsBindingSource.MoveNext();


        }

        private void BtnEnd_Click(object sender, EventArgs e)
        {
            sTATISTIC_TYPEsBindingSource.MoveLast();

        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            sTATISTIC_TYPEsBindingSource.AddNew();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
               
                db.SubmitChanges();
            }
            catch (Exception why) { MessageBox.Show(why.Message); }
        }

        private void BtnDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (stat != null)
                {
                    db.STATISTIC_TYPEs.DeleteOnSubmit((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current);
                }
                sTATISTIC_TYPEsBindingSource.RemoveCurrent();
            }
            catch { }
        }
        public STATISTIC_TYPE Current;
        private void BtnClose_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            
            FrmSelectBase frm = new FrmSelectBase(this.ConnectionString);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                int index = 0;
                foreach (var item in sTATISTIC_TYPEsBindingSource.List.OfType<STATISTIC_TYPE>())
                {
                    if(item.ID==frm.Current.ID)
                        sTATISTIC_TYPEsBindingSource.Position = index;
                    index++;
                }

            }
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtcolor_DoubleClick(object sender, EventArgs e)
        {

            if (DialogResult.OK == colorDialog1.ShowDialog())
            {
                Color cc = colorDialog1.Color;
                txtbdcolor.ForeColor = colorDialog1.Color;


                ((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).BODY_COLOR = System.Drawing.ColorTranslator.ToHtml(txtbdcolor.ForeColor);
                //Color mycolor = System.Drawing.ColorTranslator.FromHtml(string_color);
                //txtcolor.Text = string_color;
            }

        }

        private void txtfont_DoubleClick(object sender, EventArgs e)
        {
            fontDialog1.Font = txtbdfont.Font;
            if (DialogResult.OK == fontDialog1.ShowDialog())
            {
                txtbdfont.Font = fontDialog1.Font;
                var cvt = new FontConverter();

                ((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).BODY_FONT = cvt.ConvertToString(fontDialog1.Font);

               // txtfont.Text = cvt.ConvertToString(fontDialog1.Font);
              //  Font f = cvt.ConvertFromString(txtfont.Text) as Font;
            }
        }

        private void txtitlefont_DoubleClick(object sender, EventArgs e)
        {
            fontDialog1.Font = txtitlefont.Font;
            if (DialogResult.OK == fontDialog1.ShowDialog())
            {
                txtitlefont.Font = fontDialog1.Font;
                var cvt = new FontConverter();

                ((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).TITLE_FONT = cvt.ConvertToString(fontDialog1.Font);

            }
        }

        private void txtitleforecolor_DoubleClick(object sender, EventArgs e)
        {
            if (DialogResult.OK == colorDialog1.ShowDialog())
            {
                Color cc = colorDialog1.Color;
                txtitleforecolor.ForeColor = colorDialog1.Color;


                ((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).TITLE_COLOR = System.Drawing.ColorTranslator.ToHtml(txtitleforecolor.ForeColor);
            }
        }

        private void txtitlebackcolor_DoubleClick(object sender, EventArgs e)
        {
            if (DialogResult.OK == colorDialog1.ShowDialog())
            {
                Color cc = colorDialog1.Color;
                txtitlebackcolor.ForeColor = colorDialog1.Color;


                ((STATISTIC_TYPE)sTATISTIC_TYPEsBindingSource.Current).TITLE_BACKCOLOR = System.Drawing.ColorTranslator.ToHtml(txtitlebackcolor.ForeColor);
            }
        }

        private void dATA_KEYPAIR_NOTEsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                TestQuery f = new TestQuery(dATA_KEYPAIR_NOTEsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                if (DialogResult.OK == f.ShowDialog())
                {
                    dATA_KEYPAIR_NOTEsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = f.qeury;
                }
            }
        }

        private void dATA_KEYPAIR_NOTEsDataGridView_CellContentClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                TestQuery f = new TestQuery(dATA_KEYPAIR_NOTEsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                if (DialogResult.OK == f.ShowDialog())
                {
                    dATA_KEYPAIR_NOTEsDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = f.qeury;
                }
            }
        }


        private void btnsubdel_Click(object sender, EventArgs e)
        {
            try
            {
                db.DATA_KEYPAIR_NOTEs.DeleteOnSubmit((DATA_KEYPAIR_NOTE)dATA_KEYPAIR_NOTEsBindingSource.Current);
                dATA_KEYPAIR_NOTEsBindingSource.RemoveCurrent();
            }
            catch { }
        }

        private void btnsubnew_Click(object sender, EventArgs e)
        {
            dATA_KEYPAIR_NOTEsBindingSource.AddNew();
        }

     

      
    }
}
