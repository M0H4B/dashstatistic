﻿namespace dashstatistic
{
    partial class DashCustomize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label iS_DELETEDLabel;
            System.Windows.Forms.Label sTATISTIC_TYPE_CODELabel;
            System.Windows.Forms.Label sTATISTIC_TYPE_NAMELabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DashCustomize));
            GControls.Class.Tags tags2 = new GControls.Class.Tags();
            this.sTATISTIC_TYPEsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iS_DELETEDCheckBox = new System.Windows.Forms.CheckBox();
            this.sTATISTIC_TYPE_NAMETextBox = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dATA_KEYPAIR_NOTEsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dATA_KEYPAIR_NOTEsDataGridView = new GControls.UI.Grid.GGrid();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATAKEYDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATAVALUEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dATANOTEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sTATISTICTYPEIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iNSERTDATEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iSDELETEDDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sTATISTICTYPEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sTATISTIC_TYPE_LOCATION_XTextBox = new System.Windows.Forms.TextBox();
            this.sTATISTIC_TYPE_LOCATION_YTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtbdcolor = new System.Windows.Forms.TextBox();
            this.txtbdfont = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtitleforecolor = new System.Windows.Forms.TextBox();
            this.txtitlebackcolor = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtitlefont = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panelEx1 = new Stimulsoft.Controls.Win.DotNetBar.PanelEx();
            this.button1 = new System.Windows.Forms.Button();
            this.btnsubnew = new System.Windows.Forms.Button();
            this.btnsubdel = new System.Windows.Forms.Button();
            this.PLeft = new Stimulsoft.Controls.Win.DotNetBar.PanelEx();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.BtnClose = new System.Windows.Forms.Button();
            this.BtnDel = new System.Windows.Forms.Button();
            this.BtnNew = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.PTop = new Stimulsoft.Controls.Win.DotNetBar.PanelEx();
            this.lab_Count = new System.Windows.Forms.Label();
            this.BtnFirst = new System.Windows.Forms.Button();
            this.BtnNext = new System.Windows.Forms.Button();
            this.BtnLast = new System.Windows.Forms.Button();
            this.BtnEnd = new System.Windows.Forms.Button();
            this.TNumber = new System.Windows.Forms.TextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            iS_DELETEDLabel = new System.Windows.Forms.Label();
            sTATISTIC_TYPE_CODELabel = new System.Windows.Forms.Label();
            sTATISTIC_TYPE_NAMELabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sTATISTIC_TYPEsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dATA_KEYPAIR_NOTEsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dATA_KEYPAIR_NOTEsDataGridView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panelEx1.SuspendLayout();
            this.PLeft.SuspendLayout();
            this.PTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // iS_DELETEDLabel
            // 
            iS_DELETEDLabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            iS_DELETEDLabel.Location = new System.Drawing.Point(4, 89);
            iS_DELETEDLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            iS_DELETEDLabel.Name = "iS_DELETEDLabel";
            iS_DELETEDLabel.Size = new System.Drawing.Size(88, 61);
            iS_DELETEDLabel.TabIndex = 5;
            iS_DELETEDLabel.Text = "Hide From Dash";
            iS_DELETEDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sTATISTIC_TYPE_CODELabel
            // 
            sTATISTIC_TYPE_CODELabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sTATISTIC_TYPE_CODELabel.Location = new System.Drawing.Point(4, 41);
            sTATISTIC_TYPE_CODELabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            sTATISTIC_TYPE_CODELabel.Name = "sTATISTIC_TYPE_CODELabel";
            sTATISTIC_TYPE_CODELabel.Size = new System.Drawing.Size(88, 48);
            sTATISTIC_TYPE_CODELabel.TabIndex = 9;
            sTATISTIC_TYPE_CODELabel.Text = "Statistics Type";
            sTATISTIC_TYPE_CODELabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sTATISTIC_TYPE_NAMELabel
            // 
            sTATISTIC_TYPE_NAMELabel.Dock = System.Windows.Forms.DockStyle.Fill;
            sTATISTIC_TYPE_NAMELabel.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            sTATISTIC_TYPE_NAMELabel.Location = new System.Drawing.Point(4, 8);
            sTATISTIC_TYPE_NAMELabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            sTATISTIC_TYPE_NAMELabel.Name = "sTATISTIC_TYPE_NAMELabel";
            sTATISTIC_TYPE_NAMELabel.Size = new System.Drawing.Size(88, 33);
            sTATISTIC_TYPE_NAMELabel.TabIndex = 11;
            sTATISTIC_TYPE_NAMELabel.Text = "Statistics Title";
            sTATISTIC_TYPE_NAMELabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sTATISTIC_TYPEsBindingSource
            // 
            this.sTATISTIC_TYPEsBindingSource.DataSource = typeof(dashstatistic.STATISTIC_TYPE);
            this.sTATISTIC_TYPEsBindingSource.AddingNew += new System.ComponentModel.AddingNewEventHandler(this.sTATISTIC_TYPEsBindingSource_AddingNew);
            this.sTATISTIC_TYPEsBindingSource.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.itemBindingSource_BindingComplete);
            this.sTATISTIC_TYPEsBindingSource.CurrentChanged += new System.EventHandler(this.sTATISTIC_TYPEsBindingSource_CurrentChanged);
            // 
            // iS_DELETEDCheckBox
            // 
            this.iS_DELETEDCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.sTATISTIC_TYPEsBindingSource, "IS_DELETED", true));
            this.iS_DELETEDCheckBox.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iS_DELETEDCheckBox.Location = new System.Drawing.Point(100, 93);
            this.iS_DELETEDCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.iS_DELETEDCheckBox.Name = "iS_DELETEDCheckBox";
            this.iS_DELETEDCheckBox.Size = new System.Drawing.Size(89, 53);
            this.iS_DELETEDCheckBox.TabIndex = 6;
            this.iS_DELETEDCheckBox.Text = "Hidding";
            this.iS_DELETEDCheckBox.UseVisualStyleBackColor = true;
            // 
            // sTATISTIC_TYPE_NAMETextBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.sTATISTIC_TYPE_NAMETextBox, 2);
            this.sTATISTIC_TYPE_NAMETextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sTATISTIC_TYPEsBindingSource, "STATISTIC_TYPE_NAME", true));
            this.sTATISTIC_TYPE_NAMETextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sTATISTIC_TYPE_NAMETextBox.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sTATISTIC_TYPE_NAMETextBox.Location = new System.Drawing.Point(100, 12);
            this.sTATISTIC_TYPE_NAMETextBox.Margin = new System.Windows.Forms.Padding(4);
            this.sTATISTIC_TYPE_NAMETextBox.Name = "sTATISTIC_TYPE_NAMETextBox";
            this.sTATISTIC_TYPE_NAMETextBox.Size = new System.Drawing.Size(256, 25);
            this.sTATISTIC_TYPE_NAMETextBox.TabIndex = 12;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(dashstatistic.type);
            // 
            // dATA_KEYPAIR_NOTEsBindingSource
            // 
            this.dATA_KEYPAIR_NOTEsBindingSource.DataMember = "DATA_KEYPAIR_NOTEs";
            this.dATA_KEYPAIR_NOTEsBindingSource.DataSource = this.sTATISTIC_TYPEsBindingSource;
            this.dATA_KEYPAIR_NOTEsBindingSource.AddingNew += new System.ComponentModel.AddingNewEventHandler(this.dATA_KEYPAIR_NOTEsBindingSource_AddingNew_1);
            this.dATA_KEYPAIR_NOTEsBindingSource.BindingComplete += new System.Windows.Forms.BindingCompleteEventHandler(this.itemBindingSource_BindingComplete);
            // 
            // dATA_KEYPAIR_NOTEsDataGridView
            // 
            this.dATA_KEYPAIR_NOTEsDataGridView.AllowUserToAddRows = false;
            this.dATA_KEYPAIR_NOTEsDataGridView.AllowUserToDeleteRows = false;
            this.dATA_KEYPAIR_NOTEsDataGridView.AutoGenerateColumns = false;
            this.dATA_KEYPAIR_NOTEsDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dATA_KEYPAIR_NOTEsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dATA_KEYPAIR_NOTEsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.dATAKEYDataGridViewTextBoxColumn,
            this.dATAVALUEDataGridViewTextBoxColumn,
            this.dATANOTEDataGridViewTextBoxColumn,
            this.sTATISTICTYPEIDDataGridViewTextBoxColumn,
            this.iNSERTDATEDataGridViewTextBoxColumn,
            this.iSDELETEDDataGridViewCheckBoxColumn,
            this.sTATISTICTYPEDataGridViewTextBoxColumn});
            this.dATA_KEYPAIR_NOTEsDataGridView.DataSource = this.dATA_KEYPAIR_NOTEsBindingSource;
            this.dATA_KEYPAIR_NOTEsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dATA_KEYPAIR_NOTEsDataGridView.FirstColor = System.Drawing.Color.White;
            this.dATA_KEYPAIR_NOTEsDataGridView.Location = new System.Drawing.Point(45, 0);
            this.dATA_KEYPAIR_NOTEsDataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.dATA_KEYPAIR_NOTEsDataGridView.Name = "dATA_KEYPAIR_NOTEsDataGridView";
            this.dATA_KEYPAIR_NOTEsDataGridView.Size = new System.Drawing.Size(475, 168);
            this.dATA_KEYPAIR_NOTEsDataGridView.TabIndex = 14;
            this.dATA_KEYPAIR_NOTEsDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dATA_KEYPAIR_NOTEsDataGridView_CellContentClick);
            this.dATA_KEYPAIR_NOTEsDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dATA_KEYPAIR_NOTEsDataGridView_CellContentClick);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dATAKEYDataGridViewTextBoxColumn
            // 
            this.dATAKEYDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dATAKEYDataGridViewTextBoxColumn.DataPropertyName = "DATA_KEY";
            this.dATAKEYDataGridViewTextBoxColumn.HeaderText = "Data Key";
            this.dATAKEYDataGridViewTextBoxColumn.Name = "dATAKEYDataGridViewTextBoxColumn";
            // 
            // dATAVALUEDataGridViewTextBoxColumn
            // 
            this.dATAVALUEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dATAVALUEDataGridViewTextBoxColumn.DataPropertyName = "DATA_VALUE";
            this.dATAVALUEDataGridViewTextBoxColumn.HeaderText = "Data Value";
            this.dATAVALUEDataGridViewTextBoxColumn.Name = "dATAVALUEDataGridViewTextBoxColumn";
            // 
            // dATANOTEDataGridViewTextBoxColumn
            // 
            this.dATANOTEDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dATANOTEDataGridViewTextBoxColumn.DataPropertyName = "DATA_NOTE";
            this.dATANOTEDataGridViewTextBoxColumn.HeaderText = "Data Notes";
            this.dATANOTEDataGridViewTextBoxColumn.Name = "dATANOTEDataGridViewTextBoxColumn";
            // 
            // sTATISTICTYPEIDDataGridViewTextBoxColumn
            // 
            this.sTATISTICTYPEIDDataGridViewTextBoxColumn.DataPropertyName = "STATISTIC_TYPE_ID";
            this.sTATISTICTYPEIDDataGridViewTextBoxColumn.HeaderText = "STATISTIC_TYPE_ID";
            this.sTATISTICTYPEIDDataGridViewTextBoxColumn.Name = "sTATISTICTYPEIDDataGridViewTextBoxColumn";
            this.sTATISTICTYPEIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // iNSERTDATEDataGridViewTextBoxColumn
            // 
            this.iNSERTDATEDataGridViewTextBoxColumn.DataPropertyName = "INSERT_DATE";
            this.iNSERTDATEDataGridViewTextBoxColumn.HeaderText = "INSERT_DATE";
            this.iNSERTDATEDataGridViewTextBoxColumn.Name = "iNSERTDATEDataGridViewTextBoxColumn";
            this.iNSERTDATEDataGridViewTextBoxColumn.Visible = false;
            // 
            // iSDELETEDDataGridViewCheckBoxColumn
            // 
            this.iSDELETEDDataGridViewCheckBoxColumn.DataPropertyName = "IS_DELETED";
            this.iSDELETEDDataGridViewCheckBoxColumn.HeaderText = "IS_DELETED";
            this.iSDELETEDDataGridViewCheckBoxColumn.Name = "iSDELETEDDataGridViewCheckBoxColumn";
            this.iSDELETEDDataGridViewCheckBoxColumn.Visible = false;
            // 
            // sTATISTICTYPEDataGridViewTextBoxColumn
            // 
            this.sTATISTICTYPEDataGridViewTextBoxColumn.DataPropertyName = "STATISTIC_TYPE";
            this.sTATISTICTYPEDataGridViewTextBoxColumn.HeaderText = "STATISTIC_TYPE";
            this.sTATISTICTYPEDataGridViewTextBoxColumn.Name = "sTATISTICTYPEDataGridViewTextBoxColumn";
            this.sTATISTICTYPEDataGridViewTextBoxColumn.Visible = false;
            // 
            // comboBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.comboBox1, 2);
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.sTATISTIC_TYPEsBindingSource, "STATISTIC_TYPE_CODE", true));
            this.comboBox1.DataSource = this.bindingSource1;
            this.comboBox1.DisplayMember = "name";
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(100, 45);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(256, 25);
            this.comboBox1.TabIndex = 15;
            this.comboBox1.ValueMember = "id";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.55357F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.44643F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 167F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159F));
            this.tableLayoutPanel1.Controls.Add(sTATISTIC_TYPE_NAMELabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.sTATISTIC_TYPE_NAMETextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.iS_DELETEDCheckBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.checkBox1, 1, 4);
            this.tableLayoutPanel1.Controls.Add(iS_DELETEDLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(sTATISTIC_TYPE_CODELabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(520, 272);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label1.Location = new System.Drawing.Point(4, 150);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 30);
            this.label1.TabIndex = 16;
            this.label1.Text = "Form Index";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.sTATISTIC_TYPEsBindingSource, "LOCATION_AUTO", true));
            this.checkBox1.Location = new System.Drawing.Point(100, 154);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(57, 22);
            this.checkBox1.TabIndex = 18;
            this.checkBox1.Text = "Auto";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.0596F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.9404F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.sTATISTIC_TYPE_LOCATION_XTextBox, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.sTATISTIC_TYPE_LOCATION_YTextBox, 1, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(100, 184);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(89, 80);
            this.tableLayoutPanel2.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "X";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 40);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 15);
            this.label3.TabIndex = 16;
            this.label3.Text = "Y";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sTATISTIC_TYPE_LOCATION_XTextBox
            // 
            this.sTATISTIC_TYPE_LOCATION_XTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sTATISTIC_TYPEsBindingSource, "LOCATION_X", true));
            this.sTATISTIC_TYPE_LOCATION_XTextBox.Location = new System.Drawing.Point(40, 4);
            this.sTATISTIC_TYPE_LOCATION_XTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.sTATISTIC_TYPE_LOCATION_XTextBox.Name = "sTATISTIC_TYPE_LOCATION_XTextBox";
            this.sTATISTIC_TYPE_LOCATION_XTextBox.Size = new System.Drawing.Size(45, 25);
            this.sTATISTIC_TYPE_LOCATION_XTextBox.TabIndex = 17;
            // 
            // sTATISTIC_TYPE_LOCATION_YTextBox
            // 
            this.sTATISTIC_TYPE_LOCATION_YTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sTATISTIC_TYPEsBindingSource, "LOCATION_Y", true));
            this.sTATISTIC_TYPE_LOCATION_YTextBox.Location = new System.Drawing.Point(40, 44);
            this.sTATISTIC_TYPE_LOCATION_YTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.sTATISTIC_TYPE_LOCATION_YTextBox.Name = "sTATISTIC_TYPE_LOCATION_YTextBox";
            this.sTATISTIC_TYPE_LOCATION_YTextBox.Size = new System.Drawing.Size(45, 25);
            this.sTATISTIC_TYPE_LOCATION_YTextBox.TabIndex = 17;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel3, 2);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.55172F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.44828F));
            this.tableLayoutPanel3.Controls.Add(this.txtbdcolor, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtbdfont, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.txtitleforecolor, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtitlebackcolor, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtitlefont, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(196, 92);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel1.SetRowSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(321, 177);
            this.tableLayoutPanel3.TabIndex = 20;
            // 
            // txtbdcolor
            // 
            this.txtbdcolor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbdcolor.Location = new System.Drawing.Point(120, 116);
            this.txtbdcolor.Name = "txtbdcolor";
            this.txtbdcolor.Size = new System.Drawing.Size(198, 25);
            this.txtbdcolor.TabIndex = 19;
            this.txtbdcolor.Text = "Color";
            this.txtbdcolor.DoubleClick += new System.EventHandler(this.txtcolor_DoubleClick);
            // 
            // txtbdfont
            // 
            this.txtbdfont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtbdfont.Location = new System.Drawing.Point(120, 147);
            this.txtbdfont.Name = "txtbdfont";
            this.txtbdfont.Size = new System.Drawing.Size(198, 25);
            this.txtbdfont.TabIndex = 19;
            this.txtbdfont.Text = "Text Body";
            this.txtbdfont.DoubleClick += new System.EventHandler(this.txtfont_DoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label4.Location = new System.Drawing.Point(4, 113);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 17);
            this.label4.TabIndex = 16;
            this.label4.Text = "Body Color";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label5.Location = new System.Drawing.Point(4, 144);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 33);
            this.label5.TabIndex = 16;
            this.label5.Text = "Body Font";
            // 
            // txtitleforecolor
            // 
            this.txtitleforecolor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtitleforecolor.Location = new System.Drawing.Point(120, 34);
            this.txtitleforecolor.Name = "txtitleforecolor";
            this.txtitleforecolor.Size = new System.Drawing.Size(198, 25);
            this.txtitleforecolor.TabIndex = 19;
            this.txtitleforecolor.Text = "Color";
            this.txtitleforecolor.DoubleClick += new System.EventHandler(this.txtitleforecolor_DoubleClick);
            // 
            // txtitlebackcolor
            // 
            this.txtitlebackcolor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtitlebackcolor.Location = new System.Drawing.Point(120, 65);
            this.txtitlebackcolor.Name = "txtitlebackcolor";
            this.txtitlebackcolor.Size = new System.Drawing.Size(198, 25);
            this.txtitlebackcolor.TabIndex = 19;
            this.txtitlebackcolor.Text = "Color";
            this.txtitlebackcolor.DoubleClick += new System.EventHandler(this.txtitlebackcolor_DoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label6.Location = new System.Drawing.Point(4, 62);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Title BackColor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label7.Location = new System.Drawing.Point(4, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "Title ForeColor";
            // 
            // txtitlefont
            // 
            this.txtitlefont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtitlefont.Location = new System.Drawing.Point(120, 3);
            this.txtitlefont.Name = "txtitlefont";
            this.txtitlefont.Size = new System.Drawing.Size(198, 25);
            this.txtitlefont.TabIndex = 19;
            this.txtitlefont.Text = "Text Body";
            this.txtitlefont.DoubleClick += new System.EventHandler(this.txtitlefont_DoubleClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.label8.Location = new System.Drawing.Point(4, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "Title Font";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 61);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dATA_KEYPAIR_NOTEsDataGridView);
            this.splitContainer1.Panel2.Controls.Add(this.panelEx1);
            this.splitContainer1.Size = new System.Drawing.Size(520, 451);
            this.splitContainer1.SplitterDistance = 272;
            this.splitContainer1.SplitterWidth = 11;
            this.splitContainer1.TabIndex = 17;
            // 
            // panelEx1
            // 
            this.panelEx1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = Stimulsoft.Controls.Win.DotNetBar.eDotNetBarStyle.Office2007;
            this.panelEx1.Controls.Add(this.button1);
            this.panelEx1.Controls.Add(this.btnsubnew);
            this.panelEx1.Controls.Add(this.btnsubdel);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Margin = new System.Windows.Forms.Padding(4);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(45, 168);
            this.panelEx1.Style.BackColor1.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = Stimulsoft.Controls.Win.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(13, 337);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(3);
            this.button1.Size = new System.Drawing.Size(124, 32);
            this.button1.TabIndex = 16;
            this.button1.Tag = "2";
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnsubnew
            // 
            this.btnsubnew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsubnew.Location = new System.Drawing.Point(6, 14);
            this.btnsubnew.Margin = new System.Windows.Forms.Padding(4);
            this.btnsubnew.Name = "btnsubnew";
            this.btnsubnew.Size = new System.Drawing.Size(33, 35);
            this.btnsubnew.TabIndex = 9;
            this.btnsubnew.Tag = "0";
            this.btnsubnew.UseVisualStyleBackColor = true;
            this.btnsubnew.Click += new System.EventHandler(this.btnsubnew_Click);
            // 
            // btnsubdel
            // 
            this.btnsubdel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsubdel.Location = new System.Drawing.Point(6, 57);
            this.btnsubdel.Margin = new System.Windows.Forms.Padding(4);
            this.btnsubdel.Name = "btnsubdel";
            this.btnsubdel.Size = new System.Drawing.Size(33, 35);
            this.btnsubdel.TabIndex = 9;
            this.btnsubdel.Tag = "0";
            this.btnsubdel.UseVisualStyleBackColor = true;
            this.btnsubdel.Click += new System.EventHandler(this.btnsubdel_Click);
            // 
            // PLeft
            // 
            this.PLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PLeft.CanvasColor = System.Drawing.SystemColors.Control;
            this.PLeft.ColorSchemeStyle = Stimulsoft.Controls.Win.DotNetBar.eDotNetBarStyle.Office2007;
            this.PLeft.Controls.Add(this.BtnSearch);
            this.PLeft.Controls.Add(this.BtnClose);
            this.PLeft.Controls.Add(this.BtnDel);
            this.PLeft.Controls.Add(this.BtnNew);
            this.PLeft.Controls.Add(this.BtnSave);
            this.PLeft.Dock = System.Windows.Forms.DockStyle.Right;
            this.PLeft.Location = new System.Drawing.Point(520, 0);
            this.PLeft.Margin = new System.Windows.Forms.Padding(4);
            this.PLeft.Name = "PLeft";
            this.PLeft.Size = new System.Drawing.Size(155, 512);
            this.PLeft.Style.BackColor1.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground;
            this.PLeft.Style.BackColor2.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PLeft.Style.Border = Stimulsoft.Controls.Win.DotNetBar.eBorderType.SingleLine;
            this.PLeft.Style.BorderColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBorder;
            this.PLeft.Style.ForeColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelText;
            this.PLeft.Style.GradientAngle = 90;
            this.PLeft.TabIndex = 18;
            // 
            // BtnSearch
            // 
            this.BtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSearch.Location = new System.Drawing.Point(13, 337);
            this.BtnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Padding = new System.Windows.Forms.Padding(3);
            this.BtnSearch.Size = new System.Drawing.Size(124, 32);
            this.BtnSearch.TabIndex = 16;
            this.BtnSearch.Tag = "2";
            this.BtnSearch.Text = "Search";
            this.BtnSearch.UseVisualStyleBackColor = true;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_Click);
            // 
            // BtnClose
            // 
            this.BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnClose.Location = new System.Drawing.Point(13, 464);
            this.BtnClose.Margin = new System.Windows.Forms.Padding(4);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Padding = new System.Windows.Forms.Padding(3);
            this.BtnClose.Size = new System.Drawing.Size(124, 32);
            this.BtnClose.TabIndex = 13;
            this.BtnClose.Tag = "0";
            this.BtnClose.Text = "Close";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // BtnDel
            // 
            this.BtnDel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnDel.Location = new System.Drawing.Point(13, 132);
            this.BtnDel.Margin = new System.Windows.Forms.Padding(4);
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Padding = new System.Windows.Forms.Padding(3);
            this.BtnDel.Size = new System.Drawing.Size(124, 32);
            this.BtnDel.TabIndex = 11;
            this.BtnDel.Text = "Delete";
            this.BtnDel.UseVisualStyleBackColor = true;
            this.BtnDel.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // BtnNew
            // 
            this.BtnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnNew.Location = new System.Drawing.Point(13, 28);
            this.BtnNew.Margin = new System.Windows.Forms.Padding(4);
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.Padding = new System.Windows.Forms.Padding(3);
            this.BtnNew.Size = new System.Drawing.Size(124, 32);
            this.BtnNew.TabIndex = 10;
            this.BtnNew.Text = "New";
            this.BtnNew.UseVisualStyleBackColor = true;
            this.BtnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSave.Location = new System.Drawing.Point(13, 81);
            this.BtnSave.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Padding = new System.Windows.Forms.Padding(3);
            this.BtnSave.Size = new System.Drawing.Size(124, 32);
            this.BtnSave.TabIndex = 9;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // PTop
            // 
            this.PTop.CanvasColor = System.Drawing.SystemColors.Control;
            this.PTop.ColorSchemeStyle = Stimulsoft.Controls.Win.DotNetBar.eDotNetBarStyle.Office2007;
            this.PTop.Controls.Add(this.lab_Count);
            this.PTop.Controls.Add(this.BtnFirst);
            this.PTop.Controls.Add(this.BtnNext);
            this.PTop.Controls.Add(this.BtnLast);
            this.PTop.Controls.Add(this.BtnEnd);
            this.PTop.Controls.Add(this.TNumber);
            this.PTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PTop.Location = new System.Drawing.Point(0, 0);
            this.PTop.Margin = new System.Windows.Forms.Padding(4);
            this.PTop.Name = "PTop";
            this.PTop.Size = new System.Drawing.Size(520, 61);
            this.PTop.Style.BackColor1.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground;
            this.PTop.Style.BackColor2.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PTop.Style.Border = Stimulsoft.Controls.Win.DotNetBar.eBorderType.SingleLine;
            this.PTop.Style.BorderColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelBorder;
            this.PTop.Style.ForeColor.ColorSchemePart = Stimulsoft.Controls.Win.DotNetBar.eColorSchemePart.PanelText;
            this.PTop.Style.GradientAngle = 90;
            this.PTop.TabIndex = 19;
            // 
            // lab_Count
            // 
            this.lab_Count.AutoSize = true;
            this.lab_Count.Location = new System.Drawing.Point(143, 18);
            this.lab_Count.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lab_Count.Name = "lab_Count";
            this.lab_Count.Size = new System.Drawing.Size(23, 18);
            this.lab_Count.TabIndex = 10;
            this.lab_Count.Text = "---";
            // 
            // BtnFirst
            // 
            this.BtnFirst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnFirst.Location = new System.Drawing.Point(51, 12);
            this.BtnFirst.Margin = new System.Windows.Forms.Padding(4);
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.Size = new System.Drawing.Size(33, 35);
            this.BtnFirst.TabIndex = 9;
            this.BtnFirst.Tag = "0";
            this.BtnFirst.UseVisualStyleBackColor = true;
            this.BtnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnNext.Location = new System.Drawing.Point(209, 12);
            this.BtnNext.Margin = new System.Windows.Forms.Padding(4);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(33, 35);
            this.BtnNext.TabIndex = 9;
            this.BtnNext.Tag = "1";
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnLast
            // 
            this.BtnLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnLast.Location = new System.Drawing.Point(92, 12);
            this.BtnLast.Margin = new System.Windows.Forms.Padding(4);
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.Size = new System.Drawing.Size(33, 35);
            this.BtnLast.TabIndex = 7;
            this.BtnLast.Tag = "2";
            this.BtnLast.UseVisualStyleBackColor = true;
            this.BtnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // BtnEnd
            // 
            this.BtnEnd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnEnd.Location = new System.Drawing.Point(251, 12);
            this.BtnEnd.Margin = new System.Windows.Forms.Padding(4);
            this.BtnEnd.Name = "BtnEnd";
            this.BtnEnd.Size = new System.Drawing.Size(33, 35);
            this.BtnEnd.TabIndex = 7;
            this.BtnEnd.Tag = "3";
            this.BtnEnd.UseVisualStyleBackColor = true;
            this.BtnEnd.Click += new System.EventHandler(this.BtnEnd_Click);
            // 
            // TNumber
            // 
            this.TNumber.Location = new System.Drawing.Point(409, 12);
            this.TNumber.Margin = new System.Windows.Forms.Padding(4);
            this.TNumber.Name = "TNumber";
            this.TNumber.Size = new System.Drawing.Size(64, 25);
            this.TNumber.TabIndex = 6;
            // 
            // DashCustomize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(675, 512);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.PTop);
            this.Controls.Add(this.PLeft);
            this.Language = ((GControls.Class.CLanguage)(resources.GetObject("$this.Language")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "DashCustomize";
            tags2.OrgenalName = "FBase";
            tags2.Tag = null;
            this.Tag = tags2;
            this.Text = "Dash Board Customize Form";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DashCustomize_FormClosing);
            this.Load += new System.EventHandler(this.DashCustomize_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sTATISTIC_TYPEsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dATA_KEYPAIR_NOTEsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dATA_KEYPAIR_NOTEsDataGridView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panelEx1.ResumeLayout(false);
            this.PLeft.ResumeLayout(false);
            this.PTop.ResumeLayout(false);
            this.PTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource sTATISTIC_TYPEsBindingSource;
        private System.Windows.Forms.CheckBox iS_DELETEDCheckBox;
        private System.Windows.Forms.TextBox sTATISTIC_TYPE_NAMETextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.BindingSource dATA_KEYPAIR_NOTEsBindingSource;
        private GControls.UI.Grid.GGrid dATA_KEYPAIR_NOTEsDataGridView;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox sTATISTIC_TYPE_LOCATION_XTextBox;
        private System.Windows.Forms.TextBox sTATISTIC_TYPE_LOCATION_YTextBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private Stimulsoft.Controls.Win.DotNetBar.PanelEx PLeft;
        private System.Windows.Forms.Button BtnSearch;
        protected System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.Button BtnDel;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnNew;
        private Stimulsoft.Controls.Win.DotNetBar.PanelEx PTop;
        private System.Windows.Forms.Label lab_Count;
        private System.Windows.Forms.Button BtnFirst;
        private System.Windows.Forms.Button BtnNext;
        private System.Windows.Forms.Button BtnLast;
        private System.Windows.Forms.Button BtnEnd;
        public System.Windows.Forms.TextBox TNumber;
        private System.Windows.Forms.TextBox txtbdcolor;
        private System.Windows.Forms.TextBox txtbdfont;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtitleforecolor;
        private System.Windows.Forms.TextBox txtitlebackcolor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtitlefont;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnsubnew;
        private System.Windows.Forms.Button btnsubdel;
        private Stimulsoft.Controls.Win.DotNetBar.PanelEx panelEx1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATAKEYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATAVALUEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dATANOTEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTATISTICTYPEIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iNSERTDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn iSDELETEDDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sTATISTICTYPEDataGridViewTextBoxColumn;


    }
}